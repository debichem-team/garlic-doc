Source: garlic-doc
Section: doc
Priority: optional
Maintainer: Debichem Team <debichem-devel@lists.alioth.debian.org>
Uploaders: Michael Banck <mbanck@debian.org>
Build-Depends:
 debhelper-compat (= 13),
Standards-Version: 3.7.3
Homepage: http://www.zucic.org/garlic/
Vcs-Browser: https://salsa.debian.org/debichem-team/garlic-doc
Vcs-Git: https://salsa.debian.org/debichem-team/garlic-doc.git
DM-Upload-Allowed: yes

Package: garlic-doc
Architecture: all
Multi-Arch: foreign
Recommends: garlic
Description: [Chemistry] a molecular visualization program - documents
 This is the documentation package for Garlic.
 .
 Garlic is probably the most portable molecular visualization program
 in the Unix world. It's written for the investigation of membrane
 proteins. It may be used to visualize other proteins, as well as some
 geometric objects. The name should has something to do with the
 structure and operation of this program. This version of garlic
 recognizes PDB format version 2.1. Garlic may also be used to analyze
 protein sequences.
 .
 Features include (but not limited to):
  o The slab position and thickness are visible in a small window.
  o Atomic bonds as well as atoms are treated as independent drawable
    objects.
  o The atomic and bond colors depend on position. Five mapping modes
    are available (as for slab).
  o Capable to display stereo image.
  o Capable to display other geometric objects, like membrane.
  o Atomic information is available for atom covered by the mouse
    pointer. No click required, just move the mouse pointer over the
    structure!
  o Capable to load more than one structure.
  o Capable to draw Ramachandran plot, helical wheel, Venn diagram,
    averaged hydrophobicity and hydrophobic moment plot.
  o The command prompt is available at the bottom of the main window.
    It is able to display one error message and one command string.
